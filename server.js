/* ==================dependency files=====================*/

var express = require('express');
var http = require('http');
var path = require('path');
var jsonfile = require('jsonfile');
var file = './data.json'
var obj = {"name": 'rahul'}
var bodyParser = require('body-parser');
var beautify = require("json-beautify");

/* ==================dependency files end=====================*/




var app = express();
var root = path.normalize(__dirname + './../');
app.use(express.static(path.join(__dirname, 'app')));
app.use(bodyParser.json());


/* =======================api's ===========================*/
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname+ '/app/index.html'));
});

app.post('/api/file', function (req, res) {
    var file = req.body.path;
    jsonfile.writeFile(file, req.body.data, function (err) {
        if(err) return res.json({"success": false, "error": err});
        res.json({"success": true});
    })
});

app.get('/api/file', function (req, res) {
    var file = req.query.path;
    jsonfile.readFile(file, function(err, obj) {
        if(err) return res.json({"success": false, "error": err});
        res.json({"success": true, "data": obj});
    });
});

/* =======================api's ===========================*/




/* =======================server ===========================*/

var server = http.createServer(app);
var port = 8000;

server.listen(port, function () {
    console.log('server running');
});
/* =======================server ===========================*/


exports.module = exports = app; /* exporting app */