'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.version',
  'ngSanitize',
  'ngCsv'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider
    .when("/view", {
        templateUrl : "view/view.html"
    })
 
  $routeProvider.otherwise({redirectTo: '/view'});
}]);
