(function() {
   'use strict';

   var controllerId = 'homeCtrl';

   angular
      .module('myApp')
      .directive('customPopover', function () {
    return {
        restrict: 'A',
        template: '<span class="element fa fa-folder"></span>',
        scope:{
        tags: "="
       },
        link: function (scope, el, attrs) {
            scope.label = attrs.popoverLabel;
            console.log(scope.tags);
            $(el).popover({
                trigger: 'click',
                html: true,
                content: function() {
                  var str='';
                  for(var i=0; i<scope.tags.length; i++){
                 
                     var course=scope.tags[i].course;
                     var instructor=scope.tags[i].instructor;
                     var room=scope.tags[i].room;
                     str +='<span>Course:'+course+'</span><br>';
                       str +='<span>Instructor:'+instructor+'</span><br>';
                        str +='<span>Room:'+room+'</span><br>';
                          str +='<hr>';
                  }
                    return str;
                },
                placement: attrs.popoverPlacement
            });
        }
    };
})
    .controller(controllerId, homeCtrl);

   homeCtrl.$inject = ['$rootScope', '$scope', '$http','$compile'];
    $(document).on('click', function (e) {
          $('[data-toggle="popover"],[data-original-title]').each(function () {
              //the 'is' for buttons that trigger popups
              //the 'has' for icons within a button that triggers a popup
              if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
                  (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
              }

          });
      
    });
    

   function homeCtrl($rootScope, $scope, $http, $compile) {

      $scope.selectedChoice = function() {
          $(this).addClass('selected').siblings().removeClass('selected');    
          var value=$(this).find('td:first').html();
      }
    

      /*===============Get all data of Courses Instructor,Time Slot and Rooms from json files==================== */
      $http.get('/files/courses.json').then(function successCallback(response) {
					$scope.courseData = response.data; 
				}, function errorCallback(response) {
					console.log(response);
				});

      $http.get('/files/instructor.json').then(function successCallback(response) {
					$scope.instructorData = response.data; 
				}, function errorCallback(response) {
					console.log(response);
				});
      $http.get('/files/rooms.json').then(function successCallback(response) {
					$scope.roomsData = response.data; 
				}, function errorCallback(response) {
					console.log(response);
				});
      $http.get('/files/timeslot.json').then(function successCallback(response) {
        var data = response.data;
            if(!data){
              $scope.timeslotData = {
                  "first_slot": [[], [], [], [], [], [], []],
                  "second_slot":  [[], [], [], [], [], [], []],
                  "third_slot":  [[], [], [], [], [], [], []],
                  "fourth_slot":  [[], [], [], [], [], [], []],
                  "fifth_slot":  [[], [], [], [], [], [], []]
              };
          } else{
            $scope.timeslotData =data;
          }
          
          console.log($scope.timeslotData);
				}, function errorCallback(response) {
					console.log(response);
				});
    



      /**========================SaveConfig Function============================= */
      $scope.saveConfig = function() {
         var data = {
            path: "./app/files/timeslot.json",
            data: $scope.timeslotData
         }
         $http.post("/api/file", data).success(function(data) {
            console.log(data);
         });
      }

      /*===============Dragging event Funtion=================*/
      $scope.dragStart = function(ev,frm) {
        console.log(ev);
         if(frm==3){
              var indx=ev.target.id.split('room_')[1];
              $scope.selectRooms( $scope.roomsData[indx]);
         };
         ev.dataTransfer.effectAllowed = 'move';
         ev.dataTransfer.setDragImage(ev.target, 0, 0);
         return true;
      }
      $scope.dragDrop = function(row, evt) {
         console.log(evt);
          var indx=evt.target.id.split('_')[1]
          console.log(indx)
         var col = parseInt(indx);
        //  var myTop = evt.x;
        //  var myRight = evt.y;
        //  var div = $("<div />");
        //  div.attr({
        //     id: row.toString() + col.toString(),
        //     class: 'element fa fa-folder'
        //  });
        //  div.css({
        //     top: myTop,
        //     left: myRight
        //  })
        //  div.html('');
        //  $('#' + row.toString() + '_' + (col).toString()).append(div);
          $scope.addTimeSlotdata(row, col);
         return true;
      }
      $scope.allowDrop = function(ev) {
         ev.preventDefault();
      }
      /*===============/ End Of Dragging event Funtion=================*/
      /**=========================Collect Data=============================== */
      $scope.selectRooms = function(data) {
         $scope.selectedRooms = data.roomName;
      }

      $scope.selectInstructor = function(data) {
         $scope.selectedInstructor = data.instructorName;
      }

      $scope.selectCourse = function(data) {
         $scope.selectedCourse = data.courseName;
      }
      /**===================Add data in time============================ */

      $scope.addTimeSlotdata = function(row, col) {
         row = row - 1;
         col = col;
         var dataObj={
              "course": $scope.selectedCourse,
               "instructor": $scope.selectedInstructor,
               "room": $scope.selectedRooms
         }
         if (row == 0) {
            $scope.timeslotData.first_slot[col].push(dataObj);
         } 
         else if (row == 1) {
            $scope.timeslotData.second_slot[col].push(dataObj);
         }
           else if (row == 2) {
            $scope.timeslotData.third_slot[col].push(dataObj);
         }
           else if (row == 3) {
            $scope.timeslotData.fourth_slot[col].push(dataObj);
         }
           else if (row == 4) {
            $scope.timeslotData.fifth_slot[col].push(dataObj);
         }
        //  $scope.saveConfig();
        $scope.$apply();
       
        console.log($scope.timeslotData);
      }
   
      //   $scope.$watch( "timeslotData", function(oldValue, newValue) {
      //   // if (newValue) {
      //      //  $scope.timeslotData=newValue;
      //       var content=angular.element('#timeSlot');
      //       var scope=content.scope();
      //       $compile(content.contents())(scope);
      //   // }
      // },true);


      $scope.timeTableHeader = ['Time Slot', 'M', 'T', 'W', 'Th', 'F', 'S', 'Su'];
      $scope.getCsvHeader = function() {
          return $scope.timeTableHeader
      }

   }
})();